# This is a cross-platform list tracking distribution packages needed for install and tests;
# see https://docs.openstack.org/infra/bindep/ for additional information.

mysql-client [platform:dpkg !platform:debian]
mysql-server [platform:dpkg !platform:debian]
mariadb-server [platform:debian]
postgresql
postgresql-client [platform:dpkg]
libpq-dev [platform:dpkg]
